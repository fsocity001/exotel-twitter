# Exotel Assignment
[![CircleCI](https://circleci.com/bb/fsocity001/exotel-twitter/tree/master.svg?style=svg)](https://circleci.com/bb/fsocity001/exotel-twitter/tree/master) 
[![Sonar](https://sonarcloud.io/api/project_badges/measure?project=fsocity001_exotel-twitter2&metric=alert_status)](https://sonarcloud.io/dashboard?id=fsocity001_exotel-twitter2)

## Component
  - twitter-exotel Backend
  - Exotel-twitter NPM package for binddings all comman library like grpc,mysql,logging
  - Exotel client for testing backend 
  - Dockerfile & K8s (Both work independently on platform google/aws/datacenter) 
  - bitbucket-pipelines.yml (yml file for bitbucet pipeline for CI-CD)
  - .Circle CI (yml config for service mess deplyment of microservices)
  - cloudbuild (build file for REST deployment of Google builder)
  - envoy config (service discovvery)
  - sonar.properties (sonar configration for sonar scan)

## prerequest
 - nodejs (8+)
 - npm 
 - docker
 - kubectl 
 
# twitter-exotel 

## Get started 
```sh
$ sudo make
```

### How to test functions :
Test getUser Function
```sh
$ docker run -it --rm  exotel-client node client get 1 
```
Test createUser Function
```sh
$ docker run -it --rm  exotel-client node client insert Yuvraj evalsocket hello@123 
```
Test followUser Function
```sh
$ docker run -it --rm  exotel-client node client follow 1 2 
```
Test homeTimeline Function
```sh
$ docker run -it --rm  exotel-client node client home 1  
```
Test userTimeline Function
```sh
$ docker run -it --rm  exotel-client node client timeline 1  
```

Test createTweet Function
```sh
$ docker run -it --rm exotel-client node client tweet 1  hello
```

Configration  :  Change mysql configration (local)

How to start without docker :
```sh
$ #Run backend 
$ npm install (Install npm dependencies)
$ npm run build (Build typescript)
$ npm run serve (start grpc server port 8080)
$ npm run dev (start grpc server on dev mode port 8080)
$ #Run client 
$ cd client
$ export grpc_backend=35.243.198.252:80
$ npm install
$ node cleint get 1

```
How to start with docker :
```sh
$ # Run Docker container
$ docker build -t exotel-twitter .
$ docker run -d --name exotel-twitter exotel-twitter
$ # Run envoy proxy 
$ cd envoy
$ docker build -t front-envoy .
$ docker run -d --name front-envoy --link  exotel-twitter:exotel-twitter -p 8080:8080 front-envoy
$ # Run exotel client 
$ cd client
$ docker build -t exotel-client .
$ docker run -d --name exotel-client  --env grpc_backend=35.243.198.252:80(APP ENGINE) exotel-client (If you are running backend locally the use 127.0.0.1:8080)
$ docker run -d --name exotel-client  --env grpc_backend=35.221.233.116:80(Kuberneates envoy proxy) exotel-client (If you are running backend locally the use 127.0.0.1:8080)
```

How to start with kubctl : (optional)
```sh
$ # Run Docker container
$ docker build -t exotel-twitter .
$ docker run -it --name exotel-twitter -p 50051:8080 exotel-twitter
$ Replace Variable name manully in k8s/k8s/cloudbuild/deployment.yml
$ kubectl  apply -f k8s/k8s/cloudbuild/deployment.yml
```
# Usefull Links
   -  [Build Pipeline](https://circleci.com/bb/fsocity001/workflows/exotel-twitter )
   -  [Sonar Result](https://circleci.com/bb/fsocity001/workflows/exotel-twitter )

To-DO :
   - Logger is created in comman lib (connected to google stackdriver with winston) just need to write log in the backend  (error handling is not done)
   - Test cases are their just for testing demo( someone has to write test cases for all function)
   - Improve proto file by using proto third party like(struct and datetime)
   - For actual service mesh we have to divide backend into two microservice (tweet & user) and decide the envoy config 
   - I think work needed on mysql queries (didn't test for billion entries) but for now i rely on infra 
  
  