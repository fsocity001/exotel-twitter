.PHONY: build-client build-backend build-envoy-proxy 

build-client:
				docker build -t exotel-client ./client/.
				docker run --rm --name exotel-client --env grpc_backend=35.243.198.252:80 exotel-client 

build-backend: 
				docker build -t exotel-twitter .
				docker run -d --name exotel-twitter exotel-twitter
				docker build -t front-envoy ./envoy/.
				docker run -d --name front-envoy --link  exotel-twitter:exotel-twitter -p 8080:8080 front-envoy
